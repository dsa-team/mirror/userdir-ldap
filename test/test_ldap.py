#!/usr/bin/python

import unittest

import userdir_ldap.ldap as userdir_ldap


class PasswordTest(unittest.TestCase):

    def test_hash_pass(self):
        self.assertRegex(userdir_ldap.HashPass('foo'), r'^\$6\$rounds=656000\$.+\$.+')


if __name__ == '__main__':
    unittest.main()

# vim: set et sw=4 sts=4:
